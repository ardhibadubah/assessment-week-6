import { useEffect, useState } from 'react';
import moment from 'moment';
import axios from 'axios';
import './css/App.css';
import Quotes from './components/Quotes';
import Time from './components/Time';

function App() {
  const [currentTime, setCurrentTime] = useState('Hello :)');
  const [author, setAuthor] = useState('');
  const [quotes, setQuotes] = useState('');
  const [display, setDisplay] = useState({
    background: '',
    greeting: '',
    color: '',
    quotes: { quotes, author },
    time: currentTime,
  });

  useEffect(() => {
    axios('https://api.quotable.io/random').then((result) => {
      setAuthor(result.data.author);
      setQuotes(`"${result.data.content}"`);
    });
  }, []);

  setInterval(() => {
    setCurrentTime(moment().format('HH:mm'));
  }, 1000);

  useEffect(() => {
    axios('https://api.quotable.io/random').then((result) => {
      setAuthor(result.data.author);
      setQuotes(`"${result.data.content}"`);
    });

    const hours = parseFloat(currentTime);

    if (hours >= 5 && hours <= 11) {
      setDisplay(morning);
    } else if (hours >= 12 && hours <= 17) {
      setDisplay(afternoon);
    } else if (hours >= 18 && hours <= 21) {
      setDisplay(evening);
    } else if ((hours >= 22 && hours <= 23) || (hours >= 0 && hours <= 4)) {
      setDisplay(night);
    } else {
      setDisplay(loading);
    }
  }, [currentTime]);

  const content = {
    loading: {
      background: 'https://images3.alphacoders.com/621/621682.jpg',
      greeting: 'Welcome',
      color: 'dark',
      quotes: { quotes, author },
      time: currentTime,
    },
    morning: {
      background:
        'https://www.teahub.io/photos/full/294-2948802_sunrise-in-the-dolomites-4k-photograph.jpg',
      greeting: 'Good Morning',
      color: 'light',
      quotes: { quotes, author },
      time: currentTime,
    },
    afternoon: {
      background:
        'https://www.wallpapers13.com/wp-content/uploads/2019/07/Santa-Cristina-Gherd%C3%ABina-South-Tyrol-Italy-Nature-landscape-photography-4K-Ultra-HD-TV-Wallpaper-for-Desktop.jpg',
      greeting: 'Good Afternoon',
      color: 'light',
      quotes: { quotes, author },
      time: currentTime,
    },
    evening: {
      background:
        'https://data.1freewallpapers.com/download/tree-alone-dark-evening-4k.jpg',
      greeting: 'Good Evening',
      color: 'light',
      quotes: { quotes, author },
      time: currentTime,
    },
    night: {
      background: 'https://wallpaperaccess.com/full/123112.jpg',
      greeting: 'Good Night',
      color: 'light',
      quotes: { quotes, author },
      time: currentTime,
    },
  };

  const { loading, morning, afternoon, evening, night } = content;

  return (
    <div
      id='app'
      className='d-flex flex-column justify-content-between'
      style={{
        backgroundImage: `linear-gradient( rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.3) ), url(${display.background})`,
      }}>
      <Quotes display={display} />
      <Time display={display} />
    </div>
  );
}

export default App;
